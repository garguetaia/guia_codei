<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Init extends CI_Controller {
	public function __construct()
	{
		parent:: __construct ();
		$this->load->model('Test_model');
		//$this->admin_model->logged();
	}
	
	public function index()
	{
		$secction = 'inicio';
		$header = array(
			'title' => 'Pagina principal',
            'meta'  => '',
			'keywords'	=> '',
            'image_site' => 'assets/img/logo.png',
            'url' => base_url(),
            'type' => 'website'
		);
		$data = array(
			'DIR' 		=> base_url(),
			'CONTENIDO' => '<span>ola ke ace!</span>'
			//'MENU' => $this->Test_model->menu($secction) Lo pase a libraries/Site.php
		);
		
		$this->site->header($header);
		$this->site->view('pages/home', $data, $secction);
	}
	public function about($id)
	{
		$secction = 'about';
		$header = array(
			'title' => 'Pagina about',
            'meta'  => '',
			'keywords'	=> '',
            'image_site' => 'assets/img/logo.png',
            'url' => base_url(),
            'type' => 'website'
		);
		$data = array(
			'DIR' 		=> base_url(),
			'CONTENIDO' => '<span>ola ke ace about!</span>',
			//'MENU' => $this->Test_model->menu($secction),
			'usuario' => $this->Test_model->usuario($id)
		);
		
		$this->site->header($header);
		$this->site->view('pages/about', $data, $secction);
	}
	public function add_user(){
	{
			$datos = array(
				'name'	=> $this->input->post('name')
			);

			if($this->db->insert('usuario', $datos))
			{
				echo json_encode(array('band'=>1, 'msg'=>'insertado correctamente'));
			}else{
				echo json_encode(array('band'=>0, 'msg'=>'No se ha podido insertar'));
			}
		}
	}
}