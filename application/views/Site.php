<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]--><!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8">
<![endif]--><!--[if IE 8]>
<html class="no-js lt-ie9">
<![endif]--><!--[if gt IE 8]><!-->
<html lang="es" class="no-js">
<!--<![endif]-->
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <title>{TITLE}</title>
   <meta name="description" content="{META}">
   <meta name="keywords" content="{KEYWORDS}">
   <meta name="author" content="Gerardo Argueta">
   <meta name="robots" content="all">
   <meta name="geo.placename" content="México">
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   <meta name="google-site-verification" content="{KEY_GOOGLE}" />
   <meta property="og:title" content="{TITLE}">
   <meta property="og:type" content="{TYPE}">
   <meta property="og:site_name" content="{SITE}">
   <meta property="og:description" content="{META}">
   <meta property="og:image" content="{IMAGE_SITE}">
   <meta property="og:image:type" content="image/jpeg" />
   <meta property="og:image:width" content="100" />
   <meta property="og:image:height" content="100" />
   <meta property="og:url" content="{URL}">
   <link rel="canonical" href="{URL}" />
   <link rel="shortcut icon" href="{DIR}assets/img/{FAVICON}" type="image/ico">
</head>
<body>
   <input type="hidden" value="{DIR}" id="hdn_baseurl">
   {MENU}
   {CONTENT}
   <footer>
     <h6>© 2016 IA</h6>    
   </footer>
   <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
   <script type="text/javascript" src="{DIR}assets/js/main.js"></script>
   <script>
     {ANALYTICS}
   </script>
</body>
</html>