<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Site {

    var $meta       = '';
    var $title      = '';
    var $keygoogle  = '';
    var $site       = '';
    var $image_site = '';
    var $keywords   = '';
    var $favicon    = 'favicon.ico';
    var $url        = '';
    var $type       = '';
    var $analytics  = "";

    public function view($page = '', $data = array(), $secction = '')
    {
        $CI =& get_instance();
        if(empty($data)) $data = array('DIR'=>$CI->config->base_url());
        $site['DIR'] = base_url();
        $site['META'] = $this->meta;
        $site['TITLE'] = $this->title;
        $site['KEY_GOOGLE'] = $this->keygoogle;
        $site['SITE'] = $this->site;
        $site['IMAGE_SITE'] = base_url().$this->image_site;
        $site['KEYWORDS'] = $this->keywords;
        $site['FAVICON'] = $this->favicon;
        $site['URL'] = $this->url;
        $site['ANALYTICS'] = $this->analytics;
        $site['TYPE'] = $this->type;
        $site['MENU'] = $this->menu($secction);
        $site['CONTENT'] = $CI->parser->parse($page, $data, TRUE);
        return $CI->parser->parse('site', $site);
    }

    public function header($data=array()) {
        if(isset($data['meta']) && $data['meta'] != '')
            $this->meta = $data['meta'];
        if(isset($data['title']) && $data['title'] != '')
            $this->title = $data['title'];
        if(isset($data['main_title']) && $data['main_title'] != '')
            $this->main_title = $data['main_title'];
        if(isset($data['site']) && $data['site'] != '')
            $this->site = $data['site'];
        if(isset($data['image_site']) && $data['image_site'] != '')
            $this->image_site = $data['image_site'];
        if(isset($data['keywords']) && $data['keywords'] != '')
            $this->keywords = $data['keywords'];
        if(isset($data['favicon']) && $data['favicon'] != '')
            $this->favicon = $data['favicon'];
        if(isset($data['url']) && $data['url'] != '')
            $this->url = $data['url'];
        if(isset($data['analytics']) && $data['analytics'] != '')
            $this->analytics = $data['analytics'];
    }
    private function menu($secction){
        $html = '<p>Pagina: '.$secction.' </p><p><a href="'.base_url().'">Index</a><br />
      <a href="'.base_url().'about-us/1">About</a></p>';
        return $html;
    }
}