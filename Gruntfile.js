//Gruntfile.js
module.exports = function (grunt) {
  grunt.initConfig({
      
    // Watch task config
    watch: {
      cssmin: {
        files: ["assets/css/*.css","!css/*.min.css"],
        tasks: ['cssmin']
      },
      uglify: {
        files: ["assets/js/*.js", "!js/*.min.js"],
        tasks: ['uglify']
      },
      imagemin: {
        files: ['assets/img/*.{png,jpg,gif,svg}'],
        tasks: ['newer:imagemin'],
        options: {
          spawn: false,
        }
       }
    },
      
    // BrowserSync task config
    browserSync: {
      default_options: {
        bsFiles: {
          src: [
            "assets/css/*.css",
            "assets/js/*.js",
            "application/views/*.php",
            "application/views/pages/*.php",
            "application/controllers/*.php",
            "application/models/*.php",
            "application/libraries/*.php",
            "application/config/*.php"
          ]
        },
        options: {
          watchTask: true,
          proxy: "http://127.0.0.1/codei/"
          }
        }
      },
      
    // UnCSS task config  
    uncss: {
        dist: {
            options: {
               //Estilos que queremos limpiar
               stylesheets : ['assets/css/materialize.min.css', 'assets/css/sidebar.css', 'assets/css/main.css'],
                
               //Estilos que no queremos limpiar
               //ignoreSheets: [/custom.css/], 
            },
            files: {
                    //Archivo css de salida    //Scanea las clases, ids, etc de este html
                    'assets/css/materialize.min.css': ['application/views/*.php'],
                    'assets/css/sidebar.css': ['application/views/*.php'],
                    'assets/css/main.css': ['application/views/*.php']
            }
        }
    },
      
    // Cssmin task config
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {//Fichero combinado   //Ficheros que vamos a combinar, 2 .css
                'assets/css/allcss.min.css': ['assets/css/materialize.min.css', 'assets/css/sidebar.css', 'assets/css/main.css']
        }
      }
    },
      
    //Uglify task config
    uglify: {
      my_target: {
        files: {
        'assets/js/main.min.js': ['assets/js/jquery-2.1.1.min.js', 'assets/js/materialize.min.js', 'assets/js/sidebar.js','assets/js/main.js']
        }
      } //my_target
    },
    
    //Imagemin task config  
    imagemin: {
        main: {
          files: [{
            expand: true,
            cwd: 'assets/img/', //todas las imágenes de esta ruta
            src: ['**/*.{png,jpg,gif,.svg}'], //patrón de tipos de imagen
            dest: 'assets/img/' //carpeta destino una vez optimizadas
          }]
        }
     }
 
  });
  
  //Cargamos los grunt plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-newer');
 
 //Tarea por defecto
 grunt.registerTask('default', ['browserSync', 'watch']);
};